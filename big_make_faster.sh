#http://askubuntu.com/questions/595418/how-to-prevent-duplicate-bind-mounts
function is_mounted() {
    dir=$(echo "$1" | sed -e 's/\/\/*/\//g; s/\/$//g;')
    cut -d ' ' -f 2 /proc/mounts  | grep "^$dir$" >/dev/null
}


sudo /etc/init.d/mysql stop
sudo umount /var/ramfs
sudo mkdir -p /var/ramfs
#mount | grep "/var/ramfs" && echo "PROGRESS: looks like /var/ramfs is already mounted, something is fishy"; umount /var/ramfs;sudo mount -t ramfs -o size=5G ramfs /var/ramfs/ || sudo mount -t ramfs -o size=5G ramfs /var/ramfs/
if is_mounted '/var/ramfs'; then
    echo "YES: it's mounted"
    echo "PROGRESS: looks like /var/ramfs is already mounted, refusing to mount again"
else
    sudo mount -t ramfs -o size=5G ramfs /var/ramfs/
    echo "PROGRESS: mounting ramfs on /var/ramfs of size 5Gb"
fi
#sudo service mysql-server stop
sudo /etc/init.d/mysql stop
sudo mkdir -p /var/ramfs/log/
sudo cp -R /var/lib/mysql /var/ramfs/
sudo chown -R mysql:mysql /var/ramfs/
sudo chown -R mysql:mysql /var/ramfs/
sudo chmod -R 755 /var/lib/mysql/
sudo chmod -R 755 /var/ramfs/
sudo chmod -R 755 /var/run/mysqld/
#in file /etc/mysql/my.cnf
#Find line with 'datadir' definition(it will look something like datadir = /var/lib/mysql) and change it to
#datadir = /var/ramfs/mysql
#sudo cp /etc/mysql/my.cnf /etc/mysql/my.cnf-orig
sudo sed -i'-orig' 's/datadir.*.=.*/datadir = \/var\/ramfs\/mysql/g' /etc/mysql/my.cnf
sudo sed -i'-orig' 's/tmpdir.*.=.*/tmpdir = \/var\/ramfs/g' /etc/mysql/my.cnf

# yjk: uncomment the two lines below if binary logs should be kept in RAMDISK also e.g. for replication purposes
#      if you use bin log, then also comment out the lines below that disable bin logging
#sudo sed -i'-orig' 's/log_bin. \+=.*/log_bin = \/var\/ramfs\/log\/mariadb-bin/g' /etc/mysql/my.cnf
#sudo sed -i'-orig' 's/log_bin_index.*.=.*/log_bin_index = \/var\/ramfs\/log\/mariadb-bin.index/g' /etc/mysql/my.cnf

# yjk: disabling all bin logs for mysql alltogether to save space and increase file I/O
# BEGIN disable bin log
sudo sed -i.bak -r -e 's/(log_bin)/#\1/g' "$(echo /etc/mysql/my.cnf)"
sudo sed -i.bak -r -e 's/(log_bin_index)/#\1/g' "$(echo /etc/mysql/my.cnf)"
sudo sed -i.bak -r -e 's/(expire_logs_days)/#\1/g' "$(echo /etc/mysql/my.cnf)"
sudo sed -i.bak -r -e 's/(max_binlog_size)/#\1/g' "$(echo /etc/mysql/my.cnf)"
# END disable bin log

#Next step is to tune apparmor settings:

#sudo vim /etc/apparmor.d/usr.sbin.mysqld
##Add the following few lines just before the closing curly braces:
#/var/ramfs/mysql/ r,
#/var/ramfs/mysql/*.pid rw,
#/var/ramfs/mysql/** rwk,
#echo "  /var/ramfs/mysql/ r,
#  /var/ramfs/mysql/*.pid rw,
#  /var/ramfs/mysql/** rwk," > mod_apparmor_usr.sbin.mysqld
#sudo grep -q '/var/ramfs/mysql' /etc/apparmor.d/usr.sbin.mysqld && echo "PROGRESS: apparmor profile already modified nothing to do here " || sudo sed -i'-orig' '/  \/var\/lib\/mysql\/\*\* rwk,/r mod_apparmor_usr.sbin.mysqld' /etc/apparmor.d/usr.sbin.mysqld
#
#Looks like we're done with settings, let's see if it will work:

#sudo /etc/init.d/apparmor restart
#disable sanity checking using df which will fail for ramfs
sudo grep -q '#if LC_ALL=C BLOCKSIZE= df' /etc/init.d/mysql && echo "PROGRESS: already using modified mysql init.d script bypassing diskspace check" || sudo cp /etc/init.d/mysql /etc/init.d/dis.mysql; sudo cp init.mysql /etc/init.d/mysql
sudo /etc/init.d/mysql start
mysql -e "show variables where Variable_name = 'datadir';"
mysql -e "show variables where Variable_name = 'tmpdir';"

#If mysql daemon starts(double check /var/log/mysql.err for any errors)
#and you can connect to it, most likely now we're running fully off of a RAM device.
#To double check it, run this from mysql client:
#mysql -e "show variables where Variable_name = 'datadir';"
